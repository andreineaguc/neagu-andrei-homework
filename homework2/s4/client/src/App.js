import React, { Component } from 'react';
import ProductList from './components/ProductList.js';
import AddProduct from './components/AddProduct.js';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products:[]
    };
  }
  
  onProductAdded(product) {
    let myProducts = this.state.products;
    console.log(product);
    myProducts.push(product);
    this.setState({
      products: myProducts
    });
  }
  
  componentDidMount() {
    const url = "https://tw-final-ddey.c9users.io:8081/get-all";
    fetch(url)
    .then(res => {
      return res.json();
    })
    .then((products) =>{
      console.log(products);
      this.setState({
        products: products
      });
    })
    .catch(err => {
      console.log(err);
    });
  }
  
  render() {
    return (
      <div>
          <h1>Products List Homework</h1>
          <AddProduct handleListAdd={this.onProductAdded.bind(this)}/>
          <ProductList products={this.state.products}/>
      </div>
    );
  }
}

export default App;
