import React, { Component } from 'react';

class ProductList extends Component {
    constructor(props) {
        super(props);
        this.state = {
          products: []  
        };
    }
    
  render() {
      let list = this.props.products.map((product, index) => 
                <li key={index}>{product.productName} | Price: {product.price}$</li> 
            );
    return (
      <div>
            <h1>Products list</h1>
            <ul>
            {list}
            </ul>
      </div>
    );
  }
}

export default ProductList;
