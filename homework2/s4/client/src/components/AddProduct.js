import React, { Component } from 'react';
import axios from 'axios';

class AddProduct extends Component {
    constructor(props) {
        super(props);
        this.state = {
            productName: '',
            price: 0
        };
    }
    
    onChange(e) {
        this.setState({
           [e.target.name]: e.target.value
        });
    }
    
    onChangeProductName(e) {
        this.setState({
            productName: e.target.value
        });
        console.log(e.target.value);
    }
    
    onChangePrice(e) {
        this.setState({
            price: e.target.value
        });
        console.log(e.target.value);
    }
    
    onSave(e) {
        let product = {
            productName: this.state.productName,
            price: this.state.price
        };
        
        axios
            .post("https://tw-final-ddey.c9users.io:8081/add", product)
            .then(res => {
                if (res.status === 200) {
                    this.props.handleListAdd(product);
                }
            })
            .catch(err => {
                console.log(err);
            })
        // console.log(product);
        // this.props.handleListAdd(product);
        this.clearState();
        
    }
    
    clearState() {
        this.setState({
            productName: '',
            price: 0
        });
    }
    
  render() {
    return (
      <div>
      <h1> Add Product </h1>
        <div>
            <input type="text" placeholder="Product Name" value={this.state.productName} onChange={this.onChangeProductName.bind(this)}/>
            <input type="number" value={this.state.price} onChange={this.onChangePrice.bind(this)}/>
            <button onClick={this.onSave.bind(this)}>Add</button>
        </div>
      </div>
    );
  }
}

export default AddProduct;
