const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

let products = [
    {
        id: 0,
        productName: 'Samsung Galaxy S9',
        price: 4000
    },
    {
        id: 1,
        productName: 'Iphone XR',
        price: 3999
    }
];

app.get('/get-all', (req, res) => {
    res.status(200).send(products);
})

app.post('/add', (req, res) => {
    if(req.body.productName && req.body.price){
        let product = {
            id: products.length,
            productName: req.body.productName,
            price: req.body.price
        };
        products.push(product);
        res.status(200).send(product);
    } else {
        res.status(500).send('Error!');
    }
});

app.put('/update/:id', (req,res) => {
    const id = req.params.id;
    var isSuccessful = false;
    products.forEach(product => {
       if (product.id == id) {
           if (req.body.productName) {
                 product.productName = req.body.productName;
               }
               if (req.body.price) {
                 product.price = req.body.price;
               }
               isSuccessful = true;
       } 
    });
    
    if (isSuccessful) {
        res.status(200).send(`Product with id ${id} was successfully updated!`);
    } else {
        res.status(404).send(`Product with id ${id} was not found!`);
    }
});

app.delete('/delete', (req,res) => {
       const index = req.body.id - 1;
       if (index < products.length) {
           products.splice(index, 1);
           res.status(200).send(`Product with id ${index+1} successfully deleted`);
       } else {
           res.status(404).send(`Couldn't find any product.`);
       }
});
app.listen(8080, () => {
    console.log('Server started on port 8080...');
});